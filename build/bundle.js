/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/App.ts":
/*!********************!*\
  !*** ./src/App.ts ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! body-parser */ "body-parser");
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _controllers_handlers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./controllers/handlers */ "./src/controllers/handlers/index.ts");
/* harmony import */ var _controllers_resizeRoutes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./controllers/resizeRoutes */ "./src/controllers/resizeRoutes.ts");
/* harmony import */ var _controllers_reportRoutes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./controllers/reportRoutes */ "./src/controllers/reportRoutes.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./config */ "./src/config/index.ts");





const pretty = __webpack_require__(/*! express-prettify */ "express-prettify");

const log = Object(_config__WEBPACK_IMPORTED_MODULE_5__["debug"])('AppMain');
class App {
    constructor() {
        this.express = express__WEBPACK_IMPORTED_MODULE_0__();
        this.express.use(pretty({ query: 'pretty' }));
        this.express.use(body_parser__WEBPACK_IMPORTED_MODULE_1__["urlencoded"]({ 'extended': true }));
        this.express.use(body_parser__WEBPACK_IMPORTED_MODULE_1__["json"]());
        this.mountRoutes();
    }
    mountRoutes() {
        const router = express__WEBPACK_IMPORTED_MODULE_0__["Router"]();
        router.get('/', (req, res) => {
            // throw new Error("")
            res.json({
                message: "Hello World"
            });
        });
        this.express.use('/', router);
        router.use('/image', _controllers_resizeRoutes__WEBPACK_IMPORTED_MODULE_3__["ResizeApi"]);
        router.use('/reports', _controllers_reportRoutes__WEBPACK_IMPORTED_MODULE_4__["ReportsApi"]);
        router.use(_controllers_handlers__WEBPACK_IMPORTED_MODULE_2__["errorHandler"]);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (new App().express);


/***/ }),

/***/ "./src/config/debug.ts":
/*!*****************************!*\
  !*** ./src/config/debug.ts ***!
  \*****************************/
/*! exports provided: debug */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "debug", function() { return debug; });
const debug = __webpack_require__(/*! debug */ "debug");



/***/ }),

/***/ "./src/config/devConfig.ts":
/*!*********************************!*\
  !*** ./src/config/devConfig.ts ***!
  \*********************************/
/*! exports provided: devConfig, Config */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "devConfig", function() { return devConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Config", function() { return Config; });
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! path */ "path");
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _logging__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./logging */ "./src/config/logging.ts");


var appRoot = __webpack_require__(/*! app-root-path */ "app-root-path");
class Config {
    constructor() {
        this.log = new _logging__WEBPACK_IMPORTED_MODULE_1__["LogController"]();
        this.imageLibraryPath = path__WEBPACK_IMPORTED_MODULE_0__["join"](appRoot.path, 'public', 'lib');
        this.resizedImagesPath = path__WEBPACK_IMPORTED_MODULE_0__["join"](appRoot.path, 'public', 'resized');
    }
    get inDevelopment() {
        if (true) {
            return true;
        }
        else {}
    }
    get inProduction() {
        if (false) {}
        else {
            return false;
        }
    }
}
const devConfig = new Config();



/***/ }),

/***/ "./src/config/index.ts":
/*!*****************************!*\
  !*** ./src/config/index.ts ***!
  \*****************************/
/*! exports provided: devConfig, Config, debug, Util */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _devConfig__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./devConfig */ "./src/config/devConfig.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "devConfig", function() { return _devConfig__WEBPACK_IMPORTED_MODULE_0__["devConfig"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Config", function() { return _devConfig__WEBPACK_IMPORTED_MODULE_0__["Config"]; });

/* harmony import */ var _debug__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./debug */ "./src/config/debug.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "debug", function() { return _debug__WEBPACK_IMPORTED_MODULE_1__["debug"]; });

/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./util */ "./src/config/util.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Util", function() { return _util__WEBPACK_IMPORTED_MODULE_2__["Util"]; });






/***/ }),

/***/ "./src/config/logging.ts":
/*!*******************************!*\
  !*** ./src/config/logging.ts ***!
  \*******************************/
/*! exports provided: LogController, cacheResult */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogController", function() { return LogController; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cacheResult", function() { return cacheResult; });
var cacheResult;
(function (cacheResult) {
    cacheResult[cacheResult["miss"] = 0] = "miss";
    cacheResult[cacheResult["hit"] = 1] = "hit";
})(cacheResult || (cacheResult = {}));
class LogController {
    constructor() {
        this.cacheHits = 0;
        this.cacheMisses = 0;
        this.totalRequests = 0;
    }
    get originalFiles() {
        return 0;
    }
    get resizedFiles() {
        return 0;
    }
    add(cache) {
        if (cache === cacheResult.hit) {
            this.cacheHits += 1;
        }
        else {
            this.cacheMisses += 1;
        }
        this.totalRequests += 1;
    }
}



/***/ }),

/***/ "./src/config/util.ts":
/*!****************************!*\
  !*** ./src/config/util.ts ***!
  \****************************/
/*! exports provided: Util */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Util", function() { return Util; });
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);

class Util {
    static readFilesInLibrary(libraryPath) {
        return new Promise((resolve, reject) => {
            fs__WEBPACK_IMPORTED_MODULE_0__["readdir"](libraryPath, (err, items) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(items);
                }
            });
        });
    }
    static exists(imagePath) {
        return new Promise((resolve, reject) => {
            fs__WEBPACK_IMPORTED_MODULE_0__["access"](imagePath, fs__WEBPACK_IMPORTED_MODULE_0__["constants"].F_OK, (err) => {
                if (err) {
                    resolve(false);
                }
                else {
                    resolve(true);
                }
            });
        });
    }
}



/***/ }),

/***/ "./src/controllers/handlers/errorHandler.ts":
/*!**************************************************!*\
  !*** ./src/controllers/handlers/errorHandler.ts ***!
  \**************************************************/
/*! exports provided: errorHandler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "errorHandler", function() { return errorHandler; });
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../config */ "./src/config/index.ts");

const log = Object(_config__WEBPACK_IMPORTED_MODULE_0__["debug"])('errorHandler');
// require('debug').enable("errorHandler");
const errorHandler = function (err, req, res, next) {
    _config__WEBPACK_IMPORTED_MODULE_0__["debug"].enable('errorHandler');
    log(err.message);
    res.status(500);
    res.json({
        apiVersion: process.env.npm_package_version,
        error: err.message
    });
};



/***/ }),

/***/ "./src/controllers/handlers/index.ts":
/*!*******************************************!*\
  !*** ./src/controllers/handlers/index.ts ***!
  \*******************************************/
/*! exports provided: errorHandler, sendImageHandler, responseHandler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _errorHandler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./errorHandler */ "./src/controllers/handlers/errorHandler.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "errorHandler", function() { return _errorHandler__WEBPACK_IMPORTED_MODULE_0__["errorHandler"]; });

/* harmony import */ var _sendImageHandler__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sendImageHandler */ "./src/controllers/handlers/sendImageHandler.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sendImageHandler", function() { return _sendImageHandler__WEBPACK_IMPORTED_MODULE_1__["sendImageHandler"]; });

/* harmony import */ var _responseHandler__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./responseHandler */ "./src/controllers/handlers/responseHandler.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "responseHandler", function() { return _responseHandler__WEBPACK_IMPORTED_MODULE_2__["responseHandler"]; });






/***/ }),

/***/ "./src/controllers/handlers/responseHandler.ts":
/*!*****************************************************!*\
  !*** ./src/controllers/handlers/responseHandler.ts ***!
  \*****************************************************/
/*! exports provided: responseHandler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "responseHandler", function() { return responseHandler; });
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../config */ "./src/config/index.ts");

const log = Object(_config__WEBPACK_IMPORTED_MODULE_0__["debug"])('responseHandler');
function responseHandler(req, res, next) {
    _config__WEBPACK_IMPORTED_MODULE_0__["debug"].enable('responseHandler');
    // log("got to res.json")
    res.json({
        apiVersion: process.env.npm_package_version,
        data: res.locals
    });
}



/***/ }),

/***/ "./src/controllers/handlers/sendImageHandler.ts":
/*!******************************************************!*\
  !*** ./src/controllers/handlers/sendImageHandler.ts ***!
  \******************************************************/
/*! exports provided: sendImageHandler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendImageHandler", function() { return sendImageHandler; });
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../config */ "./src/config/index.ts");

const log = Object(_config__WEBPACK_IMPORTED_MODULE_0__["debug"])('sendImageHandler');
function sendImageHandler(req, res, next) {
    _config__WEBPACK_IMPORTED_MODULE_0__["debug"].enable('sendImageHandler');
    log(res.locals.result);
    res.type('image/jpeg');
    res.sendFile(res.locals.result);
    // ({
    //     apiVersion:process.env.npm_package_version,
    //     data:res.locals
    // })
}



/***/ }),

/***/ "./src/controllers/reportRoutes.ts":
/*!*****************************************!*\
  !*** ./src/controllers/reportRoutes.ts ***!
  \*****************************************/
/*! exports provided: ReportsApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsApi", function() { return ReportsApi; });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _reports__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reports */ "./src/controllers/reports/index.ts");
/* harmony import */ var _handlers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./handlers */ "./src/controllers/handlers/index.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config */ "./src/config/index.ts");




const log = Object(_config__WEBPACK_IMPORTED_MODULE_3__["debug"])('resizeRoutes');
_config__WEBPACK_IMPORTED_MODULE_3__["debug"].enable('resizeRoutes');
const router = Object(express__WEBPACK_IMPORTED_MODULE_0__["Router"])();
router.route('/').get(_reports__WEBPACK_IMPORTED_MODULE_1__["reportsController"], _handlers__WEBPACK_IMPORTED_MODULE_2__["responseHandler"]);
const ReportsApi = router;


/***/ }),

/***/ "./src/controllers/reports/index.ts":
/*!******************************************!*\
  !*** ./src/controllers/reports/index.ts ***!
  \******************************************/
/*! exports provided: reportsController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportController */ "./src/controllers/reports/reportController.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "reportsController", function() { return _reportController__WEBPACK_IMPORTED_MODULE_0__["reportsController"]; });




/***/ }),

/***/ "./src/controllers/reports/reportController.ts":
/*!*****************************************************!*\
  !*** ./src/controllers/reports/reportController.ts ***!
  \*****************************************************/
/*! exports provided: reportsController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reportsController", function() { return reportsController; });
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../config */ "./src/config/index.ts");
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};

const log = Object(_config__WEBPACK_IMPORTED_MODULE_0__["debug"])('reportController');
const reportsController = function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        _config__WEBPACK_IMPORTED_MODULE_0__["debug"].enable('reportController');
        try {
            let filesInLibrary = yield _config__WEBPACK_IMPORTED_MODULE_0__["Util"].readFilesInLibrary(_config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].imageLibraryPath);
            let resizedFiles = yield _config__WEBPACK_IMPORTED_MODULE_0__["Util"].readFilesInLibrary(_config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].resizedImagesPath);
            res.locals.resizedFiles = resizedFiles.length;
            res.locals.originalFiles = filesInLibrary.length;
            res.locals.cache = {
                hits: _config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].log.cacheHits,
                misses: _config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].log.cacheMisses,
                total: _config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].log.totalRequests
            };
            res.locals.success = true;
            log("Went through reportController");
            next();
        }
        catch (err) {
            next(err);
        }
    });
};



/***/ }),

/***/ "./src/controllers/resize/index.ts":
/*!*****************************************!*\
  !*** ./src/controllers/resize/index.ts ***!
  \*****************************************/
/*! exports provided: listItemsInLib, resizeImageController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _resizeImageController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./resizeImageController */ "./src/controllers/resize/resizeImageController.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "resizeImageController", function() { return _resizeImageController__WEBPACK_IMPORTED_MODULE_0__["resizeImageController"]; });

/* harmony import */ var _listFilesInLib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./listFilesInLib */ "./src/controllers/resize/listFilesInLib.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "listItemsInLib", function() { return _listFilesInLib__WEBPACK_IMPORTED_MODULE_1__["listItemsInLib"]; });





/***/ }),

/***/ "./src/controllers/resize/listFilesInLib.ts":
/*!**************************************************!*\
  !*** ./src/controllers/resize/listFilesInLib.ts ***!
  \**************************************************/
/*! exports provided: listItemsInLib */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "listItemsInLib", function() { return listItemsInLib; });
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../config */ "./src/config/index.ts");
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};

const log = Object(_config__WEBPACK_IMPORTED_MODULE_0__["debug"])('listItemsInLib');
const listItemsInLib = function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        _config__WEBPACK_IMPORTED_MODULE_0__["debug"].enable('listItemsInLib');
        try {
            let filesInLibrary = yield _config__WEBPACK_IMPORTED_MODULE_0__["Util"].readFilesInLibrary(_config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].imageLibraryPath);
            res.locals.availableImages = filesInLibrary;
            res.locals.success = true;
            log("Went through listItemsInLib");
            next();
        }
        catch (err) {
            next(err);
        }
    });
};



/***/ }),

/***/ "./src/controllers/resize/resizeImageController.ts":
/*!*********************************************************!*\
  !*** ./src/controllers/resize/resizeImageController.ts ***!
  \*********************************************************/
/*! exports provided: resizeImageController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resizeImageController", function() { return resizeImageController; });
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../config */ "./src/config/index.ts");
/* harmony import */ var _config_logging__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../config/logging */ "./src/config/logging.ts");
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! path */ "path");
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var sharp__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sharp */ "sharp");
/* harmony import */ var sharp__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sharp__WEBPACK_IMPORTED_MODULE_3__);
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};




const log = Object(_config__WEBPACK_IMPORTED_MODULE_0__["debug"])('resizeImageController');
const resizeImageController = function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        _config__WEBPACK_IMPORTED_MODULE_0__["debug"].enable('resizeImageController');
        try {
            let imageName = req.params.imageName;
            let imagePath = path__WEBPACK_IMPORTED_MODULE_2__["join"](_config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].imageLibraryPath, imageName);
            let sizeQuery = req.query.size;
            let width, height;
            if (yield _config__WEBPACK_IMPORTED_MODULE_0__["Util"].exists(imagePath)) {
                if (sizeQuery) {
                    res.locals.sizeQuery = sizeQuery;
                    width = parseInt(sizeQuery.split('x')[0]);
                    height = parseInt(sizeQuery.split('x')[1]);
                    res.locals.size = { height: height, width: width };
                    let resultingImageName = `${path__WEBPACK_IMPORTED_MODULE_2__["parse"](imageName).name}_${width}x${height}${path__WEBPACK_IMPORTED_MODULE_2__["parse"](imageName).ext}`;
                    res.locals.resultingImageName = resultingImageName;
                    //TODO: this is unsafe, an attacker can inject another path in the sistem, should be sanitized
                    let resultOutputPath = path__WEBPACK_IMPORTED_MODULE_2__["join"](_config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].resizedImagesPath, resultingImageName);
                    if (!(yield _config__WEBPACK_IMPORTED_MODULE_0__["Util"].exists(resultOutputPath))) {
                        //TODO: there is no max height or max with specified, high DOS risk.
                        let response = yield sharp__WEBPACK_IMPORTED_MODULE_3__(imagePath)
                            .resize(width, height)
                            .toFile(resultOutputPath);
                        log(response);
                        res.locals.result = resultOutputPath;
                        //add some basic stats
                        _config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].log.add(_config_logging__WEBPACK_IMPORTED_MODULE_1__["cacheResult"].miss);
                    }
                    else {
                        log("Apparently it exists");
                        res.locals.result = resultOutputPath;
                        //add some basic stats
                        _config__WEBPACK_IMPORTED_MODULE_0__["devConfig"].log.add(_config_logging__WEBPACK_IMPORTED_MODULE_1__["cacheResult"].hit);
                    }
                }
                else {
                    res.locals.result = imagePath;
                }
            }
            else {
                throw new Error("Cannot find file specified!");
            }
            log("Went through resizeImageController");
            next();
        }
        catch (err) {
            next(err);
        }
    });
};



/***/ }),

/***/ "./src/controllers/resizeRoutes.ts":
/*!*****************************************!*\
  !*** ./src/controllers/resizeRoutes.ts ***!
  \*****************************************/
/*! exports provided: ResizeApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResizeApi", function() { return ResizeApi; });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _resize__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./resize */ "./src/controllers/resize/index.ts");
/* harmony import */ var _handlers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./handlers */ "./src/controllers/handlers/index.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config */ "./src/config/index.ts");




const log = Object(_config__WEBPACK_IMPORTED_MODULE_3__["debug"])('resizeRoutes');
_config__WEBPACK_IMPORTED_MODULE_3__["debug"].enable('resizeRoutes');
const router = Object(express__WEBPACK_IMPORTED_MODULE_0__["Router"])();
router.route('/list').get(_resize__WEBPACK_IMPORTED_MODULE_1__["listItemsInLib"], _handlers__WEBPACK_IMPORTED_MODULE_2__["responseHandler"]);
router.route('/get/:imageName').get(_resize__WEBPACK_IMPORTED_MODULE_1__["resizeImageController"], _handlers__WEBPACK_IMPORTED_MODULE_2__["sendImageHandler"]);
const ResizeApi = router;


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App */ "./src/App.ts");

const port = process.env.port || 3000;
_App__WEBPACK_IMPORTED_MODULE_0__["default"].listen(port, (err) => {
    if (err) {
        return console.error(err);
    }
    return console.log(`Server started on ${port}`);
});


/***/ }),

/***/ "app-root-path":
/*!********************************!*\
  !*** external "app-root-path" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("app-root-path");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "debug":
/*!************************!*\
  !*** external "debug" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("debug");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "express-prettify":
/*!***********************************!*\
  !*** external "express-prettify" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express-prettify");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),

/***/ "sharp":
/*!************************!*\
  !*** external "sharp" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sharp");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL0FwcC50cyIsIndlYnBhY2s6Ly8vLi9zcmMvY29uZmlnL2RlYnVnLnRzIiwid2VicGFjazovLy8uL3NyYy9jb25maWcvZGV2Q29uZmlnLnRzIiwid2VicGFjazovLy8uL3NyYy9jb25maWcvaW5kZXgudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbmZpZy9sb2dnaW5nLnRzIiwid2VicGFjazovLy8uL3NyYy9jb25maWcvdXRpbC50cyIsIndlYnBhY2s6Ly8vLi9zcmMvY29udHJvbGxlcnMvaGFuZGxlcnMvZXJyb3JIYW5kbGVyLnRzIiwid2VicGFjazovLy8uL3NyYy9jb250cm9sbGVycy9oYW5kbGVycy9pbmRleC50cyIsIndlYnBhY2s6Ly8vLi9zcmMvY29udHJvbGxlcnMvaGFuZGxlcnMvcmVzcG9uc2VIYW5kbGVyLnRzIiwid2VicGFjazovLy8uL3NyYy9jb250cm9sbGVycy9oYW5kbGVycy9zZW5kSW1hZ2VIYW5kbGVyLnRzIiwid2VicGFjazovLy8uL3NyYy9jb250cm9sbGVycy9yZXBvcnRSb3V0ZXMudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbnRyb2xsZXJzL3JlcG9ydHMvaW5kZXgudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbnRyb2xsZXJzL3JlcG9ydHMvcmVwb3J0Q29udHJvbGxlci50cyIsIndlYnBhY2s6Ly8vLi9zcmMvY29udHJvbGxlcnMvcmVzaXplL2luZGV4LnRzIiwid2VicGFjazovLy8uL3NyYy9jb250cm9sbGVycy9yZXNpemUvbGlzdEZpbGVzSW5MaWIudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbnRyb2xsZXJzL3Jlc2l6ZS9yZXNpemVJbWFnZUNvbnRyb2xsZXIudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbnRyb2xsZXJzL3Jlc2l6ZVJvdXRlcy50cyIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXgudHMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiYXBwLXJvb3QtcGF0aFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcImJvZHktcGFyc2VyXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZGVidWdcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJleHByZXNzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZXhwcmVzcy1wcmV0dGlmeVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcImZzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicGF0aFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInNoYXJwXCIiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBbUM7QUFDTTtBQUNVO0FBQ0M7QUFDQztBQUNyRCxNQUFNLE1BQU0sR0FBRyxtQkFBTyxDQUFDLDBDQUFrQixDQUFDLENBQUM7QUFDWjtBQUMvQixNQUFNLEdBQUcsR0FBRyxxREFBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBRTdCLE1BQU0sR0FBRztJQUdMO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxvQ0FBTyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0RBQXFCLENBQUMsRUFBQyxVQUFVLEVBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdEQUFlLEVBQUUsQ0FBQyxDQUFDO1FBRXBDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRU8sV0FBVztRQUNmLE1BQU0sTUFBTSxHQUFHLDhDQUFjLEVBQUUsQ0FBQztRQUVoQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQW1CLEVBQUMsR0FBb0IsRUFBQyxFQUFFO1lBQ3hELHNCQUFzQjtZQUN0QixHQUFHLENBQUMsSUFBSSxDQUFDO2dCQUNMLE9BQU8sRUFBQyxhQUFhO2FBQ3hCLENBQUM7UUFDTixDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDOUIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsbUVBQVMsQ0FBQyxDQUFDO1FBQ2hDLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLG9FQUFVLENBQUMsQ0FBQztRQUNuQyxNQUFNLENBQUMsR0FBRyxDQUFDLGtFQUFZLENBQUMsQ0FBQztJQUU3QixDQUFDO0NBQ0o7QUFFYyxtRUFBSSxHQUFHLEVBQUUsQ0FBQyxPQUFPOzs7Ozs7Ozs7Ozs7O0FDdkNoQztBQUFBO0FBQUEsTUFBTSxLQUFLLEdBQUcsbUJBQU8sQ0FBQyxvQkFBTyxDQUFDLENBQUM7QUFFakI7Ozs7Ozs7Ozs7Ozs7QUNGZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBNEI7QUFDVztBQUN2QyxJQUFJLE9BQU8sR0FBRyxtQkFBTyxDQUFDLG9DQUFlLENBQUMsQ0FBQztBQUV2QyxNQUFNLE1BQU07SUFJUjtRQUNJLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxzREFBYSxFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLHlDQUFTLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLHlDQUFTLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDO0lBQ3pFLENBQUM7SUFFRCxJQUFJLGFBQWE7UUFDYixJQUFHLElBQXlFLEVBQzVFO1lBQ0ksT0FBTyxJQUFJO1NBQ2Q7YUFBSSxFQUVKO0lBQ0wsQ0FBQztJQUNELElBQUksWUFBWTtRQUNaLElBQUcsS0FBbUMsRUFDdEMsRUFFQzthQUFJO1lBQ0QsT0FBTyxLQUFLO1NBQ2Y7SUFDTCxDQUFDO0NBRUo7QUFFRCxNQUFNLFNBQVMsR0FBRyxJQUFJLE1BQU0sRUFBRTtBQUNIOzs7Ozs7Ozs7Ozs7O0FDbEMzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBNEI7QUFDSjtBQUNGOzs7Ozs7Ozs7Ozs7O0FDRnRCO0FBQUE7QUFBQTtBQUFBLElBQUssV0FHSjtBQUhELFdBQUssV0FBVztJQUNaLDZDQUFRO0lBQ1IsMkNBQU87QUFDWCxDQUFDLEVBSEksV0FBVyxLQUFYLFdBQVcsUUFHZjtBQUVELE1BQU0sYUFBYTtJQUtmO1FBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQztRQUNwQixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUM7SUFDMUIsQ0FBQztJQUNELElBQUksYUFBYTtRQUNiLE9BQU8sQ0FBQztJQUNaLENBQUM7SUFDRCxJQUFJLFlBQVk7UUFDWixPQUFPLENBQUM7SUFDWixDQUFDO0lBRUQsR0FBRyxDQUFDLEtBQWlCO1FBQ2pCLElBQUcsS0FBSyxLQUFHLFdBQVcsQ0FBQyxHQUFHLEVBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsSUFBRSxDQUFDLENBQUM7U0FDckI7YUFBSTtZQUNELElBQUksQ0FBQyxXQUFXLElBQUUsQ0FBQyxDQUFDO1NBQ3ZCO1FBQ0QsSUFBSSxDQUFDLGFBQWEsSUFBRSxDQUFDLENBQUM7SUFDMUIsQ0FBQztDQUVKO0FBR2tDOzs7Ozs7Ozs7Ozs7O0FDbENuQztBQUFBO0FBQUE7QUFBQTtBQUF3QjtBQUl4QixNQUFNLElBQUk7SUFDTixNQUFNLENBQUMsa0JBQWtCLENBQUMsV0FBa0I7UUFDeEMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUMsRUFBRTtZQUNsQywwQ0FBVSxDQUFDLFdBQVcsRUFBQyxDQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUMsRUFBRTtnQkFDaEMsSUFBRyxHQUFHLEVBQUM7b0JBQ0gsTUFBTSxDQUFDLEdBQUcsQ0FBQztpQkFDZDtxQkFBSTtvQkFDRCxPQUFPLENBQUMsS0FBSyxDQUFDO2lCQUNqQjtZQUNMLENBQUMsQ0FBQztRQUNOLENBQUMsQ0FBQztJQUNOLENBQUM7SUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQWdCO1FBQzFCLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFDLEVBQUU7WUFDbEMseUNBQVMsQ0FBQyxTQUFTLEVBQUUsNENBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxHQUFHLEVBQUMsRUFBRTtnQkFDM0MsSUFBRyxHQUFHLEVBQUM7b0JBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQztpQkFDakI7cUJBQUk7b0JBQ0QsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNqQjtZQUNMLENBQUMsQ0FBQztRQUNOLENBQUMsQ0FBQztJQUNOLENBQUM7Q0FDSjtBQUdZOzs7Ozs7Ozs7Ozs7O0FDNUJiO0FBQUE7QUFBQTtBQUFtQztBQUVuQyxNQUFNLEdBQUcsR0FBRyxxREFBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBRWxDLDJDQUEyQztBQUUzQyxNQUFNLFlBQVksR0FBRyxVQUFTLEdBQVMsRUFBRSxHQUFXLEVBQUMsR0FBWSxFQUFDLElBQWlCO0lBQzNFLDZDQUFLLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQztJQUU1QixHQUFHLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQztJQUVoQixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2hCLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDTCxVQUFVLEVBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUI7UUFDMUMsS0FBSyxFQUFDLEdBQUcsQ0FBQyxPQUFPO0tBQ3BCLENBQUM7QUFJVixDQUFDO0FBRW9COzs7Ozs7Ozs7Ozs7O0FDdkJyQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUE4QjtBQUNJO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7QUNEakM7QUFBQTtBQUFBO0FBQW1DO0FBRW5DLE1BQU0sR0FBRyxHQUFHLHFEQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQztBQUVyQyxTQUFTLGVBQWUsQ0FBRSxHQUFXLEVBQUMsR0FBWSxFQUFDLElBQWlCO0lBQzVELDZDQUFLLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDO0lBQy9CLHlCQUF5QjtJQUN6QixHQUFHLENBQUMsSUFBSSxDQUFDO1FBQ0wsVUFBVSxFQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CO1FBQzFDLElBQUksRUFBQyxHQUFHLENBQUMsTUFBTTtLQUNsQixDQUFDO0FBSVYsQ0FBQztBQUV1Qjs7Ozs7Ozs7Ozs7OztBQ2hCeEI7QUFBQTtBQUFBO0FBQW1DO0FBRW5DLE1BQU0sR0FBRyxHQUFHLHFEQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQztBQUV0QyxTQUFTLGdCQUFnQixDQUFDLEdBQVcsRUFBQyxHQUFZLEVBQUMsSUFBaUI7SUFDNUQsNkNBQUssQ0FBQyxNQUFNLENBQUMsa0JBQWtCLENBQUM7SUFDaEMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ3RCLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQ3RCLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDL0IsS0FBSztJQUNMLGtEQUFrRDtJQUNsRCxzQkFBc0I7SUFDdEIsS0FBSztBQUNiLENBQUM7QUFFd0I7Ozs7Ozs7Ozs7Ozs7QUNoQnpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQW9EO0FBSWxDO0FBQzJDO0FBQzdCO0FBR2hDLE1BQU0sR0FBRyxHQUFHLHFEQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7QUFDbEMsNkNBQUssQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7QUFFN0IsTUFBTSxNQUFNLEdBQVcsc0RBQU0sRUFBRSxDQUFDO0FBRWhDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLDBEQUFpQixFQUFFLHlEQUFlLENBQUM7QUFFbEQsTUFBTSxVQUFVLEdBQVcsTUFBTSxDQUFDOzs7Ozs7Ozs7Ozs7O0FDaEJ6QztBQUFBO0FBQUE7QUFBQTtBQUFrQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQ2dCO0FBR2xELE1BQU0sR0FBRyxHQUFHLHFEQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQztBQUl0QyxNQUFNLGlCQUFpQixHQUFJLFVBQWUsR0FBWSxFQUFFLEdBQVksRUFBRSxJQUFpQjs7UUFFbkYsNkNBQUssQ0FBQyxNQUFNLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNqQyxJQUFHO1lBRUMsSUFBSSxjQUFjLEdBQW1CLE1BQU0sNENBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpREFBUyxDQUFDLGdCQUFnQixDQUFDO1lBQzlGLElBQUksWUFBWSxHQUFtQixNQUFNLDRDQUFJLENBQUMsa0JBQWtCLENBQUMsaURBQVMsQ0FBQyxpQkFBaUIsQ0FBQztZQUM3RixHQUFHLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDO1lBQzlDLEdBQUcsQ0FBQyxNQUFNLENBQUMsYUFBYSxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUM7WUFDakQsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUc7Z0JBQ2YsSUFBSSxFQUFDLGlEQUFTLENBQUMsR0FBRyxDQUFDLFNBQVM7Z0JBQzVCLE1BQU0sRUFBQyxpREFBUyxDQUFDLEdBQUcsQ0FBQyxXQUFXO2dCQUNoQyxLQUFLLEVBQUMsaURBQVMsQ0FBQyxHQUFHLENBQUMsYUFBYTthQUNwQztZQUNELEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUMxQixHQUFHLENBQUMsK0JBQStCLENBQUM7WUFDcEMsSUFBSSxFQUFFO1NBRVQ7UUFBQSxPQUFNLEdBQUcsRUFBQztZQUNQLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNiO0lBQ0wsQ0FBQztDQUFBO0FBSTBCOzs7Ozs7Ozs7Ozs7O0FDakMzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF1QztBQUNQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBa0I7QUFHbEQsTUFBTSxHQUFHLEdBQUcscURBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0FBSXBDLE1BQU0sY0FBYyxHQUFJLFVBQWUsR0FBWSxFQUFFLEdBQVksRUFBRSxJQUFpQjs7UUFFaEYsNkNBQUssQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUMvQixJQUFHO1lBRUMsSUFBSSxjQUFjLEdBQUcsTUFBTSw0Q0FBSSxDQUFDLGtCQUFrQixDQUFDLGlEQUFTLENBQUMsZ0JBQWdCLENBQUM7WUFDOUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxlQUFlLEdBQUcsY0FBYyxDQUFDO1lBQzVDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUMxQixHQUFHLENBQUMsNkJBQTZCLENBQUM7WUFDbEMsSUFBSSxFQUFFO1NBRVQ7UUFBQSxPQUFNLEdBQUcsRUFBQztZQUNQLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNiO0lBQ0wsQ0FBQztDQUFBO0FBSXVCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pCNEI7QUFDSjtBQUNwQjtBQUNFO0FBQzlCLE1BQU0sR0FBRyxHQUFHLHFEQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQztBQUUzQyxNQUFNLHFCQUFxQixHQUFHLFVBQWUsR0FBWSxFQUFFLEdBQVksRUFBRSxJQUFpQjs7UUFFdEYsNkNBQUssQ0FBQyxNQUFNLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUN0QyxJQUFHO1lBQ0MsSUFBSSxTQUFTLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7WUFDckMsSUFBSSxTQUFTLEdBQUcseUNBQVMsQ0FBQyxpREFBUyxDQUFDLGdCQUFnQixFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQ2pFLElBQUksU0FBUyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBQy9CLElBQUksS0FBSyxFQUFFLE1BQU07WUFNakIsSUFBRyxNQUFNLDRDQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFDO2dCQUU1QixJQUFHLFNBQVMsRUFBQztvQkFFVCxHQUFHLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxTQUFTO29CQUNoQyxLQUFLLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDMUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzNDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFDLEVBQUMsTUFBTSxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUMsS0FBSyxFQUFDO29CQUM1QyxJQUFJLGtCQUFrQixHQUFHLEdBQUcsMENBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLElBQUksS0FBSyxJQUFJLE1BQU0sR0FBRywwQ0FBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRTtvQkFDdkcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztvQkFFbkQsOEZBQThGO29CQUM5RixJQUFJLGdCQUFnQixHQUFHLHlDQUFTLENBQUMsaURBQVMsQ0FBQyxpQkFBaUIsRUFBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUVqRixJQUFHLENBQUMsT0FBTSw0Q0FBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFDO3dCQUNwQyxvRUFBb0U7d0JBQ3BFLElBQUksUUFBUSxHQUFHLE1BQU0sa0NBQUssQ0FBQyxTQUFTLENBQUM7NkJBQ2hDLE1BQU0sQ0FBQyxLQUFLLEVBQUMsTUFBTSxDQUFDOzZCQUNwQixNQUFNLENBQUMsZ0JBQWdCLENBQUM7d0JBQzdCLEdBQUcsQ0FBQyxRQUFRLENBQUM7d0JBQ2IsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUM7d0JBQ3JDLHNCQUFzQjt3QkFDdEIsaURBQVMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLDJEQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3ZDO3lCQUFJO3dCQUNELEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQzt3QkFDM0IsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUM7d0JBQ3JDLHNCQUFzQjt3QkFDdEIsaURBQVMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLDJEQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ3RDO2lCQUVKO3FCQUFJO29CQUNELEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQztpQkFDakM7YUFHSjtpQkFBSTtnQkFDRCxNQUFNLElBQUksS0FBSyxDQUFDLDZCQUE2QixDQUFDO2FBQ2pEO1lBRUQsR0FBRyxDQUFDLG9DQUFvQyxDQUFDO1lBSXpDLElBQUksRUFBRTtTQUVUO1FBQUEsT0FBTSxHQUFHLEVBQUM7WUFDUCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDYjtJQUNMLENBQUM7Q0FBQTtBQUk4Qjs7Ozs7Ozs7Ozs7OztBQ3hFL0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBb0Q7QUFLbkM7QUFDNEM7QUFDN0I7QUFHaEMsTUFBTSxHQUFHLEdBQUcscURBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztBQUNsQyw2Q0FBSyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztBQUU3QixNQUFNLE1BQU0sR0FBVyxzREFBTSxFQUFFLENBQUM7QUFFaEMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsc0RBQWMsRUFBRSx5REFBZSxDQUFDO0FBQzFELE1BQU0sQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxHQUFHLENBQUMsNkRBQXFCLEVBQUUsMERBQWdCLENBQUM7QUFFckUsTUFBTSxTQUFTLEdBQVcsTUFBTSxDQUFDOzs7Ozs7Ozs7Ozs7O0FDbEJ4QztBQUFBO0FBQXVCO0FBRXZCLE1BQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQztBQUV0Qyw0Q0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxHQUFTLEVBQUMsRUFBRTtJQUMxQixJQUFHLEdBQUcsRUFBQztRQUNILE9BQU8sT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUM3QjtJQUVELE9BQU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsSUFBSSxFQUFFLENBQUM7QUFDbkQsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7QUNWRiwwQzs7Ozs7Ozs7Ozs7QUNBQSx3Qzs7Ozs7Ozs7Ozs7QUNBQSxrQzs7Ozs7Ozs7Ozs7QUNBQSxvQzs7Ozs7Ozs7Ozs7QUNBQSw2Qzs7Ozs7Ozs7Ozs7QUNBQSwrQjs7Ozs7Ozs7Ozs7QUNBQSxpQzs7Ozs7Ozs7Ozs7QUNBQSxrQyIsImZpbGUiOiJidW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC50c1wiKTtcbiIsImltcG9ydCAqIGFzIGV4cHJlc3MgZnJvbSBcImV4cHJlc3NcIjtcbmltcG9ydCAqIGFzIGJvZHlQYXJzZXIgZnJvbSBcImJvZHktcGFyc2VyXCJcbmltcG9ydCB7ZXJyb3JIYW5kbGVyfSBmcm9tIFwiLi9jb250cm9sbGVycy9oYW5kbGVyc1wiXG5pbXBvcnQge1Jlc2l6ZUFwaX0gZnJvbSBcIi4vY29udHJvbGxlcnMvcmVzaXplUm91dGVzXCJcbmltcG9ydCB7UmVwb3J0c0FwaX0gZnJvbSBcIi4vY29udHJvbGxlcnMvcmVwb3J0Um91dGVzXCJcbmNvbnN0IHByZXR0eSA9IHJlcXVpcmUoJ2V4cHJlc3MtcHJldHRpZnknKTtcbmltcG9ydCB7ZGVidWd9IGZyb20gXCIuL2NvbmZpZ1wiO1xuY29uc3QgbG9nID0gZGVidWcoJ0FwcE1haW4nKTtcblxuY2xhc3MgQXBwe1xuICAgIHB1YmxpYyBleHByZXNzOmV4cHJlc3MuRXhwcmVzcztcblxuICAgIGNvbnN0cnVjdG9yKCl7XG4gICAgICAgIHRoaXMuZXhwcmVzcyA9IGV4cHJlc3MoKTtcbiAgICAgICAgdGhpcy5leHByZXNzLnVzZShwcmV0dHkoeyBxdWVyeTogJ3ByZXR0eScgfSkpXG4gICAgICAgIHRoaXMuZXhwcmVzcy51c2UoYm9keVBhcnNlci51cmxlbmNvZGVkKHsnZXh0ZW5kZWQnOnRydWV9KSk7XG4gICAgICAgIHRoaXMuZXhwcmVzcy51c2UoYm9keVBhcnNlci5qc29uKCkpO1xuXG4gICAgICAgIHRoaXMubW91bnRSb3V0ZXMoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIG1vdW50Um91dGVzKCk6IHZvaWQge1xuICAgICAgICBjb25zdCByb3V0ZXIgPSBleHByZXNzLlJvdXRlcigpO1xuXG4gICAgICAgIHJvdXRlci5nZXQoJy8nLCAocmVxOmV4cHJlc3MuUmVxdWVzdCxyZXM6ZXhwcmVzcy5SZXNwb25zZSk9PntcbiAgICAgICAgICAgIC8vIHRocm93IG5ldyBFcnJvcihcIlwiKVxuICAgICAgICAgICAgcmVzLmpzb24oe1xuICAgICAgICAgICAgICAgIG1lc3NhZ2U6XCJIZWxsbyBXb3JsZFwiXG4gICAgICAgICAgICB9KVxuICAgICAgICB9KVxuXG4gICAgICAgIHRoaXMuZXhwcmVzcy51c2UoJy8nLCByb3V0ZXIpO1xuICAgICAgICByb3V0ZXIudXNlKCcvaW1hZ2UnLCBSZXNpemVBcGkpO1xuICAgICAgICByb3V0ZXIudXNlKCcvcmVwb3J0cycsIFJlcG9ydHNBcGkpO1xuICAgICAgICByb3V0ZXIudXNlKGVycm9ySGFuZGxlcik7XG5cbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IG5ldyBBcHAoKS5leHByZXNzIiwiY29uc3QgZGVidWcgPSByZXF1aXJlKCdkZWJ1ZycpO1xuXG5leHBvcnQge2RlYnVnfSIsImltcG9ydCAqIGFzIHBhdGggZnJvbSBcInBhdGhcIlxuaW1wb3J0IHtMb2dDb250cm9sbGVyfSBmcm9tIFwiLi9sb2dnaW5nXCJcbnZhciBhcHBSb290ID0gcmVxdWlyZSgnYXBwLXJvb3QtcGF0aCcpO1xuXG5jbGFzcyBDb25maWd7XG4gICAgaW1hZ2VMaWJyYXJ5UGF0aDpzdHJpbmdcbiAgICByZXNpemVkSW1hZ2VzUGF0aDpzdHJpbmdcbiAgICBsb2c6TG9nQ29udHJvbGxlclxuICAgIGNvbnN0cnVjdG9yKCl7ICBcbiAgICAgICAgdGhpcy5sb2cgPSBuZXcgTG9nQ29udHJvbGxlcigpOyAgIFxuICAgICAgICB0aGlzLmltYWdlTGlicmFyeVBhdGggPSBwYXRoLmpvaW4oYXBwUm9vdC5wYXRoLCAncHVibGljJywgJ2xpYicpO1xuICAgICAgICB0aGlzLnJlc2l6ZWRJbWFnZXNQYXRoID0gcGF0aC5qb2luKGFwcFJvb3QucGF0aCwgJ3B1YmxpYycsICdyZXNpemVkJylcbiAgICB9XG5cbiAgICBnZXQgaW5EZXZlbG9wbWVudCgpe1xuICAgICAgICBpZihwcm9jZXNzLmVudi5OT0RFX0VOVj09PVwiZGV2ZWxvcG1lbnRcIiB8fCBwcm9jZXNzLmVudi5OT0RFX0VOVj09XCJ1bmRlZmluZWRcIilcbiAgICAgICAge1xuICAgICAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgICAgfVxuICAgIH1cbiAgICBnZXQgaW5Qcm9kdWN0aW9uKCl7XG4gICAgICAgIGlmKHByb2Nlc3MuZW52Lk5PREVfRU5WPT09XCJwcm9kdWN0aW9uXCIpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH1lbHNle1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICAgIH1cbiAgICB9XG5cbn1cblxuY29uc3QgZGV2Q29uZmlnID0gbmV3IENvbmZpZygpXG5leHBvcnQge2RldkNvbmZpZywgQ29uZmlnfTsiLCJleHBvcnQgKiBmcm9tIFwiLi9kZXZDb25maWdcIjtcbmV4cG9ydCAqIGZyb20gXCIuL2RlYnVnXCI7XG5leHBvcnQgKiBmcm9tIFwiLi91dGlsXCJcbiIsImVudW0gY2FjaGVSZXN1bHR7XG4gICAgbWlzcyA9IDAsXG4gICAgaGl0ID0gMVxufVxuXG5jbGFzcyBMb2dDb250cm9sbGVye1xuICAgIGNhY2hlSGl0czpudW1iZXJcbiAgICBjYWNoZU1pc3NlczpudW1iZXJcbiAgICB0b3RhbFJlcXVlc3RzOm51bWJlclxuICAgIFxuICAgIGNvbnN0cnVjdG9yKCl7XG4gICAgICAgIHRoaXMuY2FjaGVIaXRzID0gMFxuICAgICAgICB0aGlzLmNhY2hlTWlzc2VzID0gMFxuICAgICAgICB0aGlzLnRvdGFsUmVxdWVzdHMgPSAwXG4gICAgfVxuICAgIGdldCBvcmlnaW5hbEZpbGVzKCl7XG4gICAgICAgIHJldHVybiAwXG4gICAgfVxuICAgIGdldCByZXNpemVkRmlsZXMoKXtcbiAgICAgICAgcmV0dXJuIDBcbiAgICB9XG5cbiAgICBhZGQoY2FjaGU6Y2FjaGVSZXN1bHQpe1xuICAgICAgICBpZihjYWNoZT09PWNhY2hlUmVzdWx0LmhpdCl7XG4gICAgICAgICAgICB0aGlzLmNhY2hlSGl0cys9MTtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICB0aGlzLmNhY2hlTWlzc2VzKz0xO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMudG90YWxSZXF1ZXN0cys9MTtcbiAgICB9XG5cbn1cblxuXG5leHBvcnQge0xvZ0NvbnRyb2xsZXIsIGNhY2hlUmVzdWx0fSIsImltcG9ydCAqIGFzIGZzIGZyb20gXCJmc1wiXG5pbXBvcnQgeyByZXNvbHZlIH0gZnJvbSBcInVybFwiO1xuXG5cbmNsYXNzIFV0aWx7XG4gICAgc3RhdGljIHJlYWRGaWxlc0luTGlicmFyeShsaWJyYXJ5UGF0aDpzdHJpbmcpe1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCk9PntcbiAgICAgICAgICAgIGZzLnJlYWRkaXIobGlicmFyeVBhdGgsKGVycixpdGVtcyk9PntcbiAgICAgICAgICAgICAgICBpZihlcnIpe1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoZXJyKVxuICAgICAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGl0ZW1zKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pICBcbiAgICAgICAgfSlcbiAgICB9XG4gICAgc3RhdGljIGV4aXN0cyhpbWFnZVBhdGg6c3RyaW5nKXtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpPT57XG4gICAgICAgICAgICBmcy5hY2Nlc3MoaW1hZ2VQYXRoLCBmcy5jb25zdGFudHMuRl9PSywgKGVycik9PntcbiAgICAgICAgICAgICAgICBpZihlcnIpe1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGZhbHNlKVxuICAgICAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pXG4gICAgfVxufVxuXG5cbmV4cG9ydCB7VXRpbH0iLCJpbXBvcnQgeyBSb3V0ZXIsIFJlcXVlc3QsIFJlc3BvbnNlLCBOZXh0RnVuY3Rpb24gfSBmcm9tICdleHByZXNzJztcblxuaW1wb3J0IHtkZWJ1Z30gZnJvbSBcIi4uLy4uL2NvbmZpZ1wiO1xuXG5jb25zdCBsb2cgPSBkZWJ1ZygnZXJyb3JIYW5kbGVyJyk7XG5cbi8vIHJlcXVpcmUoJ2RlYnVnJykuZW5hYmxlKFwiZXJyb3JIYW5kbGVyXCIpO1xuXG5jb25zdCBlcnJvckhhbmRsZXIgPSBmdW5jdGlvbihlcnI6RXJyb3IgLHJlcTpSZXF1ZXN0LHJlczpSZXNwb25zZSxuZXh0Ok5leHRGdW5jdGlvbil7XG4gICAgICAgIGRlYnVnLmVuYWJsZSgnZXJyb3JIYW5kbGVyJylcblxuICAgICAgICBsb2coZXJyLm1lc3NhZ2UpXG4gICAgICAgIFxuICAgICAgICByZXMuc3RhdHVzKDUwMCk7XG4gICAgICAgIHJlcy5qc29uKHtcbiAgICAgICAgICAgIGFwaVZlcnNpb246cHJvY2Vzcy5lbnYubnBtX3BhY2thZ2VfdmVyc2lvbixcbiAgICAgICAgICAgIGVycm9yOmVyci5tZXNzYWdlXG4gICAgICAgIH0pXG5cblxuXG59XG5cbmV4cG9ydCB7ZXJyb3JIYW5kbGVyfSIsImV4cG9ydCAqIGZyb20gXCIuL2Vycm9ySGFuZGxlclwiXG5leHBvcnQgKiBmcm9tIFwiLi9zZW5kSW1hZ2VIYW5kbGVyXCJcbmV4cG9ydCAqIGZyb20gXCIuL3Jlc3BvbnNlSGFuZGxlclwiIiwiaW1wb3J0IHsgUm91dGVyLCBSZXF1ZXN0LCBSZXNwb25zZSwgTmV4dEZ1bmN0aW9uLCBFcnJvclJlcXVlc3RIYW5kbGVyIH0gZnJvbSAnZXhwcmVzcyc7XG5pbXBvcnQge2RlYnVnfSBmcm9tIFwiLi4vLi4vY29uZmlnXCI7XG5cbmNvbnN0IGxvZyA9IGRlYnVnKCdyZXNwb25zZUhhbmRsZXInKTtcblxuZnVuY3Rpb24gcmVzcG9uc2VIYW5kbGVyIChyZXE6UmVxdWVzdCxyZXM6UmVzcG9uc2UsbmV4dDpOZXh0RnVuY3Rpb24pe1xuICAgICAgICBkZWJ1Zy5lbmFibGUoJ3Jlc3BvbnNlSGFuZGxlcicpXG4gICAgICAgIC8vIGxvZyhcImdvdCB0byByZXMuanNvblwiKVxuICAgICAgICByZXMuanNvbih7XG4gICAgICAgICAgICBhcGlWZXJzaW9uOnByb2Nlc3MuZW52Lm5wbV9wYWNrYWdlX3ZlcnNpb24sXG4gICAgICAgICAgICBkYXRhOnJlcy5sb2NhbHNcbiAgICAgICAgfSlcblxuXG5cbn1cblxuZXhwb3J0IHtyZXNwb25zZUhhbmRsZXJ9IiwiaW1wb3J0IHsgUm91dGVyLCBSZXF1ZXN0LCBSZXNwb25zZSwgTmV4dEZ1bmN0aW9uLCBFcnJvclJlcXVlc3RIYW5kbGVyIH0gZnJvbSAnZXhwcmVzcyc7XG5pbXBvcnQge2RlYnVnfSBmcm9tIFwiLi4vLi4vY29uZmlnXCI7XG5cbmNvbnN0IGxvZyA9IGRlYnVnKCdzZW5kSW1hZ2VIYW5kbGVyJyk7XG5cbmZ1bmN0aW9uIHNlbmRJbWFnZUhhbmRsZXIocmVxOlJlcXVlc3QscmVzOlJlc3BvbnNlLG5leHQ6TmV4dEZ1bmN0aW9uKXtcbiAgICAgICAgZGVidWcuZW5hYmxlKCdzZW5kSW1hZ2VIYW5kbGVyJylcbiAgICAgICAgbG9nKHJlcy5sb2NhbHMucmVzdWx0KVxuICAgICAgICByZXMudHlwZSgnaW1hZ2UvanBlZycpXG4gICAgICAgIHJlcy5zZW5kRmlsZShyZXMubG9jYWxzLnJlc3VsdClcbiAgICAgICAgLy8gKHtcbiAgICAgICAgLy8gICAgIGFwaVZlcnNpb246cHJvY2Vzcy5lbnYubnBtX3BhY2thZ2VfdmVyc2lvbixcbiAgICAgICAgLy8gICAgIGRhdGE6cmVzLmxvY2Fsc1xuICAgICAgICAvLyB9KVxufVxuXG5leHBvcnQge3NlbmRJbWFnZUhhbmRsZXJ9IiwiaW1wb3J0IHsgUm91dGVyLCBSZXF1ZXN0LCBSZXNwb25zZSB9IGZyb20gJ2V4cHJlc3MnO1xuaW1wb3J0IHtyZXF1aXJlc0F1dGh9IGZyb20gXCIuL2F1dGgvYXV0aENvbnRyb2xsZXJcIjtcbmltcG9ydCB7XG4gICAgcmVwb3J0c0NvbnRyb2xsZXJcbn0gZnJvbSBcIi4vcmVwb3J0c1wiXG5pbXBvcnQge3Jlc3BvbnNlSGFuZGxlciwgc2VuZEltYWdlSGFuZGxlcn0gZnJvbSBcIi4vaGFuZGxlcnNcIjtcbmltcG9ydCB7ZGVidWd9IGZyb20gXCIuLi9jb25maWdcIjtcblxuXG5jb25zdCBsb2cgPSBkZWJ1ZygncmVzaXplUm91dGVzJyk7XG5kZWJ1Zy5lbmFibGUoJ3Jlc2l6ZVJvdXRlcycpO1xuXG5jb25zdCByb3V0ZXI6IFJvdXRlciA9IFJvdXRlcigpO1xuXG5yb3V0ZXIucm91dGUoJy8nKS5nZXQocmVwb3J0c0NvbnRyb2xsZXIsIHJlc3BvbnNlSGFuZGxlcilcblxuZXhwb3J0IGNvbnN0IFJlcG9ydHNBcGk6IFJvdXRlciA9IHJvdXRlcjtcbiIsImV4cG9ydCAqIGZyb20gXCIuL3JlcG9ydENvbnRyb2xsZXJcIiIsImltcG9ydCB7IFJlcXVlc3QsIFJlc3BvbnNlLCBOZXh0RnVuY3Rpb24gfSBmcm9tICdleHByZXNzJztcbmltcG9ydCB7ZGV2Q29uZmlnLGRlYnVnLCBVdGlsfSBmcm9tIFwiLi4vLi4vY29uZmlnXCJcblxuaW1wb3J0ICogYXMgZnMgZnJvbSBcImZzXCJcbmNvbnN0IGxvZyA9IGRlYnVnKCdyZXBvcnRDb250cm9sbGVyJyk7XG5cbiBcblxuY29uc3QgcmVwb3J0c0NvbnRyb2xsZXIgPSAgYXN5bmMgZnVuY3Rpb24ocmVxOiBSZXF1ZXN0LCByZXM6UmVzcG9uc2UsIG5leHQ6TmV4dEZ1bmN0aW9uKXtcbiAgICBcbiAgICBkZWJ1Zy5lbmFibGUoJ3JlcG9ydENvbnRyb2xsZXInKTtcbiAgICB0cnl7XG4gICAgICAgIFxuICAgICAgICBsZXQgZmlsZXNJbkxpYnJhcnkgPSA8QXJyYXk8c3RyaW5nPj4gYXdhaXQgVXRpbC5yZWFkRmlsZXNJbkxpYnJhcnkoZGV2Q29uZmlnLmltYWdlTGlicmFyeVBhdGgpXG4gICAgICAgIGxldCByZXNpemVkRmlsZXMgPSA8QXJyYXk8c3RyaW5nPj4gYXdhaXQgVXRpbC5yZWFkRmlsZXNJbkxpYnJhcnkoZGV2Q29uZmlnLnJlc2l6ZWRJbWFnZXNQYXRoKSBcbiAgICAgICAgcmVzLmxvY2Fscy5yZXNpemVkRmlsZXMgPSByZXNpemVkRmlsZXMubGVuZ3RoO1xuICAgICAgICByZXMubG9jYWxzLm9yaWdpbmFsRmlsZXMgPSBmaWxlc0luTGlicmFyeS5sZW5ndGg7XG4gICAgICAgIHJlcy5sb2NhbHMuY2FjaGUgPSB7XG4gICAgICAgICAgICBoaXRzOmRldkNvbmZpZy5sb2cuY2FjaGVIaXRzLFxuICAgICAgICAgICAgbWlzc2VzOmRldkNvbmZpZy5sb2cuY2FjaGVNaXNzZXMsXG4gICAgICAgICAgICB0b3RhbDpkZXZDb25maWcubG9nLnRvdGFsUmVxdWVzdHNcbiAgICAgICAgfVxuICAgICAgICByZXMubG9jYWxzLnN1Y2Nlc3MgPSB0cnVlO1xuICAgICAgICBsb2coXCJXZW50IHRocm91Z2ggcmVwb3J0Q29udHJvbGxlclwiKVxuICAgICAgICBuZXh0KClcblxuICAgIH1jYXRjaChlcnIpe1xuICAgICAgICBuZXh0KGVycik7XG4gICAgfVxufVxuXG5cblxuZXhwb3J0IHtyZXBvcnRzQ29udHJvbGxlcn07XG4iLCJleHBvcnQgKiBmcm9tIFwiLi9yZXNpemVJbWFnZUNvbnRyb2xsZXJcIlxuZXhwb3J0ICogZnJvbSBcIi4vbGlzdEZpbGVzSW5MaWJcIiIsImltcG9ydCB7IFJlcXVlc3QsIFJlc3BvbnNlLCBOZXh0RnVuY3Rpb24gfSBmcm9tICdleHByZXNzJztcbmltcG9ydCB7ZGV2Q29uZmlnLGRlYnVnLCBVdGlsfSBmcm9tIFwiLi4vLi4vY29uZmlnXCJcblxuaW1wb3J0ICogYXMgZnMgZnJvbSBcImZzXCJcbmNvbnN0IGxvZyA9IGRlYnVnKCdsaXN0SXRlbXNJbkxpYicpO1xuXG4gXG5cbmNvbnN0IGxpc3RJdGVtc0luTGliID0gIGFzeW5jIGZ1bmN0aW9uKHJlcTogUmVxdWVzdCwgcmVzOlJlc3BvbnNlLCBuZXh0Ok5leHRGdW5jdGlvbil7XG4gICAgXG4gICAgZGVidWcuZW5hYmxlKCdsaXN0SXRlbXNJbkxpYicpO1xuICAgIHRyeXtcbiAgICAgICAgXG4gICAgICAgIGxldCBmaWxlc0luTGlicmFyeSA9IGF3YWl0IFV0aWwucmVhZEZpbGVzSW5MaWJyYXJ5KGRldkNvbmZpZy5pbWFnZUxpYnJhcnlQYXRoKVxuICAgICAgICByZXMubG9jYWxzLmF2YWlsYWJsZUltYWdlcyA9IGZpbGVzSW5MaWJyYXJ5O1xuICAgICAgICByZXMubG9jYWxzLnN1Y2Nlc3MgPSB0cnVlO1xuICAgICAgICBsb2coXCJXZW50IHRocm91Z2ggbGlzdEl0ZW1zSW5MaWJcIilcbiAgICAgICAgbmV4dCgpXG5cbiAgICB9Y2F0Y2goZXJyKXtcbiAgICAgICAgbmV4dChlcnIpO1xuICAgIH1cbn1cblxuXG5cbmV4cG9ydCB7bGlzdEl0ZW1zSW5MaWJ9O1xuIiwiaW1wb3J0IHsgUmVxdWVzdCwgUmVzcG9uc2UsIE5leHRGdW5jdGlvbiB9IGZyb20gJ2V4cHJlc3MnO1xuaW1wb3J0IHtkZWJ1ZywgVXRpbCwgZGV2Q29uZmlnfSBmcm9tIFwiLi4vLi4vY29uZmlnXCI7XG5pbXBvcnQge2NhY2hlUmVzdWx0fSBmcm9tIFwiLi4vLi4vY29uZmlnL2xvZ2dpbmdcIlxuaW1wb3J0ICogYXMgcGF0aCBmcm9tIFwicGF0aFwiXG5pbXBvcnQgKiBhcyBzaGFycCBmcm9tIFwic2hhcnBcIlxuY29uc3QgbG9nID0gZGVidWcoJ3Jlc2l6ZUltYWdlQ29udHJvbGxlcicpO1xuXG5jb25zdCByZXNpemVJbWFnZUNvbnRyb2xsZXIgPSBhc3luYyBmdW5jdGlvbihyZXE6IFJlcXVlc3QsIHJlczpSZXNwb25zZSwgbmV4dDpOZXh0RnVuY3Rpb24pe1xuXG4gICAgZGVidWcuZW5hYmxlKCdyZXNpemVJbWFnZUNvbnRyb2xsZXInKTtcbiAgICB0cnl7XG4gICAgICAgIGxldCBpbWFnZU5hbWUgPSByZXEucGFyYW1zLmltYWdlTmFtZTtcbiAgICAgICAgbGV0IGltYWdlUGF0aCA9IHBhdGguam9pbihkZXZDb25maWcuaW1hZ2VMaWJyYXJ5UGF0aCwgaW1hZ2VOYW1lKTtcbiAgICAgICAgbGV0IHNpemVRdWVyeSA9IHJlcS5xdWVyeS5zaXplO1xuICAgICAgICBsZXQgd2lkdGgsIGhlaWdodFxuXG5cblxuXG4gICAgICAgIFxuICAgICAgICBpZihhd2FpdCBVdGlsLmV4aXN0cyhpbWFnZVBhdGgpKXtcblxuICAgICAgICAgICAgaWYoc2l6ZVF1ZXJ5KXtcblxuICAgICAgICAgICAgICAgIHJlcy5sb2NhbHMuc2l6ZVF1ZXJ5ID0gc2l6ZVF1ZXJ5XG4gICAgICAgICAgICAgICAgd2lkdGggPSBwYXJzZUludChzaXplUXVlcnkuc3BsaXQoJ3gnKVswXSk7XG4gICAgICAgICAgICAgICAgaGVpZ2h0ID0gcGFyc2VJbnQoc2l6ZVF1ZXJ5LnNwbGl0KCd4JylbMV0pO1xuICAgICAgICAgICAgICAgIHJlcy5sb2NhbHMuc2l6ZT17aGVpZ2h0OmhlaWdodCwgd2lkdGg6d2lkdGh9XG4gICAgICAgICAgICAgICAgbGV0IHJlc3VsdGluZ0ltYWdlTmFtZSA9IGAke3BhdGgucGFyc2UoaW1hZ2VOYW1lKS5uYW1lfV8ke3dpZHRofXgke2hlaWdodH0ke3BhdGgucGFyc2UoaW1hZ2VOYW1lKS5leHR9YFxuICAgICAgICAgICAgICAgIHJlcy5sb2NhbHMucmVzdWx0aW5nSW1hZ2VOYW1lID0gcmVzdWx0aW5nSW1hZ2VOYW1lO1xuXG4gICAgICAgICAgICAgICAgLy9UT0RPOiB0aGlzIGlzIHVuc2FmZSwgYW4gYXR0YWNrZXIgY2FuIGluamVjdCBhbm90aGVyIHBhdGggaW4gdGhlIHNpc3RlbSwgc2hvdWxkIGJlIHNhbml0aXplZFxuICAgICAgICAgICAgICAgIGxldCByZXN1bHRPdXRwdXRQYXRoID0gcGF0aC5qb2luKGRldkNvbmZpZy5yZXNpemVkSW1hZ2VzUGF0aCxyZXN1bHRpbmdJbWFnZU5hbWUpO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGlmKCFhd2FpdCBVdGlsLmV4aXN0cyhyZXN1bHRPdXRwdXRQYXRoKSl7XG4gICAgICAgICAgICAgICAgICAgIC8vVE9ETzogdGhlcmUgaXMgbm8gbWF4IGhlaWdodCBvciBtYXggd2l0aCBzcGVjaWZpZWQsIGhpZ2ggRE9TIHJpc2suXG4gICAgICAgICAgICAgICAgICAgIGxldCByZXNwb25zZSA9IGF3YWl0IHNoYXJwKGltYWdlUGF0aClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZXNpemUod2lkdGgsaGVpZ2h0KVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRvRmlsZShyZXN1bHRPdXRwdXRQYXRoKVxuICAgICAgICAgICAgICAgICAgICBsb2cocmVzcG9uc2UpXG4gICAgICAgICAgICAgICAgICAgIHJlcy5sb2NhbHMucmVzdWx0ID0gcmVzdWx0T3V0cHV0UGF0aDtcbiAgICAgICAgICAgICAgICAgICAgLy9hZGQgc29tZSBiYXNpYyBzdGF0c1xuICAgICAgICAgICAgICAgICAgICBkZXZDb25maWcubG9nLmFkZChjYWNoZVJlc3VsdC5taXNzKTtcbiAgICAgICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICAgICAgbG9nKFwiQXBwYXJlbnRseSBpdCBleGlzdHNcIilcbiAgICAgICAgICAgICAgICAgICAgcmVzLmxvY2Fscy5yZXN1bHQgPSByZXN1bHRPdXRwdXRQYXRoO1xuICAgICAgICAgICAgICAgICAgICAvL2FkZCBzb21lIGJhc2ljIHN0YXRzXG4gICAgICAgICAgICAgICAgICAgIGRldkNvbmZpZy5sb2cuYWRkKGNhY2hlUmVzdWx0LmhpdCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICByZXMubG9jYWxzLnJlc3VsdCA9IGltYWdlUGF0aDtcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgIH1lbHNleyAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgZmlsZSBzcGVjaWZpZWQhXCIpXG4gICAgICAgIH1cblxuICAgICAgICBsb2coXCJXZW50IHRocm91Z2ggcmVzaXplSW1hZ2VDb250cm9sbGVyXCIpXG5cbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBuZXh0KClcblxuICAgIH1jYXRjaChlcnIpe1xuICAgICAgICBuZXh0KGVycik7XG4gICAgfVxufVxuXG5cblxuZXhwb3J0IHtyZXNpemVJbWFnZUNvbnRyb2xsZXJ9O1xuIiwiaW1wb3J0IHsgUm91dGVyLCBSZXF1ZXN0LCBSZXNwb25zZSB9IGZyb20gJ2V4cHJlc3MnO1xuaW1wb3J0IHtyZXF1aXJlc0F1dGh9IGZyb20gXCIuL2F1dGgvYXV0aENvbnRyb2xsZXJcIjtcbmltcG9ydCB7XG4gICAgcmVzaXplSW1hZ2VDb250cm9sbGVyLFxuICAgIGxpc3RJdGVtc0luTGliXG59IGZyb20gXCIuL3Jlc2l6ZVwiXG5pbXBvcnQge3Jlc3BvbnNlSGFuZGxlciwgc2VuZEltYWdlSGFuZGxlcn0gZnJvbSBcIi4vaGFuZGxlcnNcIjtcbmltcG9ydCB7ZGVidWd9IGZyb20gXCIuLi9jb25maWdcIjtcblxuXG5jb25zdCBsb2cgPSBkZWJ1ZygncmVzaXplUm91dGVzJyk7XG5kZWJ1Zy5lbmFibGUoJ3Jlc2l6ZVJvdXRlcycpO1xuXG5jb25zdCByb3V0ZXI6IFJvdXRlciA9IFJvdXRlcigpO1xuXG5yb3V0ZXIucm91dGUoJy9saXN0JykuZ2V0KGxpc3RJdGVtc0luTGliLCByZXNwb25zZUhhbmRsZXIpXG5yb3V0ZXIucm91dGUoJy9nZXQvOmltYWdlTmFtZScpLmdldChyZXNpemVJbWFnZUNvbnRyb2xsZXIsIHNlbmRJbWFnZUhhbmRsZXIpXG5cbmV4cG9ydCBjb25zdCBSZXNpemVBcGk6IFJvdXRlciA9IHJvdXRlcjtcbiIsImltcG9ydCBhcHAgZnJvbSBcIi4vQXBwXCJcblxuY29uc3QgcG9ydCA9IHByb2Nlc3MuZW52LnBvcnQgfHwgMzAwMDtcblxuYXBwLmxpc3Rlbihwb3J0LCAoZXJyOkVycm9yKT0+e1xuICAgIGlmKGVycil7XG4gICAgICAgIHJldHVybiBjb25zb2xlLmVycm9yKGVycik7XG4gICAgfVxuXG4gICAgcmV0dXJuIGNvbnNvbGUubG9nKGBTZXJ2ZXIgc3RhcnRlZCBvbiAke3BvcnR9YClcbn0pIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYXBwLXJvb3QtcGF0aFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJib2R5LXBhcnNlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJkZWJ1Z1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJleHByZXNzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImV4cHJlc3MtcHJldHRpZnlcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZnNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicGF0aFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzaGFycFwiKTsiXSwic291cmNlUm9vdCI6IiJ9