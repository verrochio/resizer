var path = require('path');
var fs = require('fs');
var webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');



module.exports = {
    entry: './src/index.ts',
    devtool: 'inline-source-map',
    target:'node',
    mode:'development',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    resolve: {
      extensions: ['.ts'] //resolve all the modules other than index.ts
    },
    externals: [nodeExternals()],
    node:{
        __dirname:false,
        __filename:false
    },
    module: {
        rules: [
            {
                use: 'ts-loader',
                test: /\.ts?$/
            }
        ]
    }
}