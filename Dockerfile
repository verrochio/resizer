FROM node:8.11.3-alpine
WORKDIR /usr/app

COPY . .

RUN npm install --production --quiet
RUN npm run build

CMD ["npm","run","start:prod"]