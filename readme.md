# Image Resizer Microservice

### Install instructions
Clone repo then:
```
# yarn
# yarn start:dev
```
Server will be available on http://localhost:3000
 

### Run Tests

```
# yarn test
```

### Docker
```
# docker-compose build
# docker-compose up
```

### Endpoints

###### Hello World endpoint
> / 

###### Reports endpoint
> /reports  
###### You can add ?pretty as a query param to see a pretty print of the result 


###### Get available image list
> /image/get/list
###### You can add ?pretty as a query param to see a pretty print of the result 
>/image/get/list?pretty

###### Get image endpoint
> /image/get/:imageName


###### Example http://localhost:3000/image/get/alfred-schrock-780651-unsplash.jpg

###### Get image endpoint
> /image/get/:imageName?size=640x480

###### Example http://localhost:3000/image/get/alfred-schrock-780651-unsplash.jpg?size=640x480

