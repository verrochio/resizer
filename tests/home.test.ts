import * as request from 'supertest';
import { expect, should } from 'chai';
import app from '../src/App';
import {} from "ts-jest"

describe('GET /', ()=>{

    it('should return 200 OK',()=>{
        return request(app)
            .get('/')
            .expect(200)
            .then(res=>{
                expect(res.body).have.property('message')
            })
    })



})

