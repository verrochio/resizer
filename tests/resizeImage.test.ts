import * as request from 'supertest';
import { expect, should } from 'chai';
import app from '../src/App';
import {} from "ts-jest"

describe('GET getDefaultRes', ()=>{

    it('should return 200 OK',(done)=>{
        return request(app)
            .get('/image/get/alfred-schrock-780651-unsplash.jpg')
            .expect(200)
            .then((res)=>{
                done()
            })

    })



})
describe('GET resizeImage', ()=>{

    it('should return 200 OK',(done)=>{
        return request(app)
            .get('/image/get/alfred-schrock-780651-unsplash.jpg?size=200x200')
            .expect(200)
            .then((res)=>{
                // console.log(res.body)
                done()
            })

    })



})
