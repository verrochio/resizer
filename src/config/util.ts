import * as fs from "fs"
import { resolve } from "url";


class Util{
    static readFilesInLibrary(libraryPath:string){
        return new Promise((resolve, reject)=>{
            fs.readdir(libraryPath,(err,items)=>{
                if(err){
                    reject(err)
                }else{
                    resolve(items)
                }
            })  
        })
    }
    static exists(imagePath:string){
        return new Promise((resolve, reject)=>{
            fs.access(imagePath, fs.constants.F_OK, (err)=>{
                if(err){
                    resolve(false)
                }else{
                    resolve(true);
                }
            })
        })
    }
}


export {Util}