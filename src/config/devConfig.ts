import * as path from "path"
import {LogController} from "./logging"
var appRoot = require('app-root-path');
import * as fs from "fs"
class Config{
    imageLibraryPath:string
    resizedImagesPath:string
    log:LogController
    constructor(){  
        this.log = new LogController(); 

        this.imageLibraryPath = path.join(appRoot.path, 'public', 'lib');
        this.resizedImagesPath = path.join(appRoot.path, 'public', 'resized')
        if (!fs.existsSync(this.resizedImagesPath)){
            fs.mkdirSync(this.resizedImagesPath);
        }
    }

    get inDevelopment(){
        if(process.env.NODE_ENV==="development" || process.env.NODE_ENV=="undefined")
        {
            return true
        }else{
            return false
        }
    }
    get inProduction(){
        if(process.env.NODE_ENV==="production")
        {
            return true
        }else{
            return false
        }
    }

}

const devConfig = new Config()
export {devConfig, Config};