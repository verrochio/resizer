enum cacheResult{
    miss = 0,
    hit = 1
}

class LogController{
    cacheHits:number
    cacheMisses:number
    totalRequests:number
    
    constructor(){
        this.cacheHits = 0
        this.cacheMisses = 0
        this.totalRequests = 0
    }
    get originalFiles(){
        return 0
    }
    get resizedFiles(){
        return 0
    }

    add(cache:cacheResult){
        if(cache===cacheResult.hit){
            this.cacheHits+=1;
        }else{
            this.cacheMisses+=1;
        }
        this.totalRequests+=1;
    }

}


export {LogController, cacheResult}