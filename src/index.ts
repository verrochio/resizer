import app from "./App"

const port = process.env.port || 3000;

app.listen(port, (err:Error)=>{
    if(err){
        return console.error(err);
    }

    return console.log(`Server started on ${port}`)
})