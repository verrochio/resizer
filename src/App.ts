import * as express from "express";
import * as bodyParser from "body-parser"
import {errorHandler} from "./controllers/handlers"
import {ResizeApi} from "./controllers/resizeRoutes"
import {ReportsApi} from "./controllers/reportRoutes"
const pretty = require('express-prettify');
import {debug} from "./config";
const log = debug('AppMain');

class App{
    public express:express.Express;

    constructor(){
        this.express = express();
        this.express.use(pretty({ query: 'pretty' }))
        this.express.use(bodyParser.urlencoded({'extended':true}));
        this.express.use(bodyParser.json());

        this.mountRoutes();
    }

    private mountRoutes(): void {
        const router = express.Router();

        router.get('/', (req:express.Request,res:express.Response)=>{
            // throw new Error("")
            res.json({
                message:"Hello World"
            })
        })

        this.express.use('/', router);
        router.use('/image', ResizeApi);
        router.use('/reports', ReportsApi);
        router.use(errorHandler);

    }
}

export default new App().express