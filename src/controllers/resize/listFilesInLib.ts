import { Request, Response, NextFunction } from 'express';
import {devConfig,debug, Util} from "../../config"

import * as fs from "fs"
const log = debug('listItemsInLib');

 

const listItemsInLib =  async function(req: Request, res:Response, next:NextFunction){
    
    debug.enable('listItemsInLib');
    try{
        
        let filesInLibrary = await Util.readFilesInLibrary(devConfig.imageLibraryPath)
        res.locals.availableImages = filesInLibrary;
        res.locals.success = true;
        log("Went through listItemsInLib")
        next()

    }catch(err){
        next(err);
    }
}



export {listItemsInLib};
