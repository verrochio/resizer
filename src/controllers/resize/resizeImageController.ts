import { Request, Response, NextFunction } from 'express';
import {debug, Util, devConfig} from "../../config";
import {cacheResult} from "../../config/logging"
import * as path from "path"
import * as sharp from "sharp"
const log = debug('resizeImageController');

const resizeImageController = async function(req: Request, res:Response, next:NextFunction){

    debug.enable('resizeImageController');
    try{
        let imageName = req.params.imageName;
        let imagePath = path.join(devConfig.imageLibraryPath, imageName);
        let sizeQuery = req.query.size;
        let width, height




        
        if(await Util.exists(imagePath)){

            if(sizeQuery){

                res.locals.sizeQuery = sizeQuery
                width = parseInt(sizeQuery.split('x')[0]);
                height = parseInt(sizeQuery.split('x')[1]);
                res.locals.size={height:height, width:width}
                let resultingImageName = `${path.parse(imageName).name}_${width}x${height}${path.parse(imageName).ext}`
                res.locals.resultingImageName = resultingImageName;

                //TODO: this is unsafe, an attacker can inject another path in the sistem, should be sanitized
                let resultOutputPath = path.join(devConfig.resizedImagesPath,resultingImageName);
                
                if(!await Util.exists(resultOutputPath)){
                    //TODO: there is no max height or max with specified, high DOS risk.
                    let response = await sharp(imagePath)
                        .resize(width,height)
                        .toFile(resultOutputPath)
                    log(response)
                    res.locals.result = resultOutputPath;
                    //add some basic stats
                    devConfig.log.add(cacheResult.miss);
                }else{
                    log("Apparently it exists")
                    res.locals.result = resultOutputPath;
                    //add some basic stats
                    devConfig.log.add(cacheResult.hit);
                }

            }else{
                res.locals.result = imagePath;
                devConfig.log.add(cacheResult.hit);

            }


        }else{            
            throw new Error("Cannot find file specified!")
        }

        log("Went through resizeImageController")

        
        
        next()

    }catch(err){
        next(err);
    }
}



export {resizeImageController};
