import { Router, Request, Response, NextFunction, ErrorRequestHandler } from 'express';
import {debug} from "../../config";

const log = debug('responseHandler');

function responseHandler (req:Request,res:Response,next:NextFunction){
        debug.enable('responseHandler')
        // log("got to res.json")
        res.json({
            apiVersion:process.env.npm_package_version,
            data:res.locals
        })



}

export {responseHandler}