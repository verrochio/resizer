import { Router, Request, Response, NextFunction } from 'express';

import {debug} from "../../config";

const log = debug('errorHandler');

// require('debug').enable("errorHandler");

const errorHandler = function(err:Error ,req:Request,res:Response,next:NextFunction){
        debug.enable('errorHandler')

        log(err.message)
        
        res.status(500);
        res.json({
            apiVersion:process.env.npm_package_version,
            error:err.message
        })



}

export {errorHandler}