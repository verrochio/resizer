import { Router, Request, Response, NextFunction, ErrorRequestHandler } from 'express';
import {debug} from "../../config";

const log = debug('sendImageHandler');

function sendImageHandler(req:Request,res:Response,next:NextFunction){
        debug.enable('sendImageHandler')
        log(res.locals.result)
        res.type('image/jpeg')
        res.sendFile(res.locals.result)
        // ({
        //     apiVersion:process.env.npm_package_version,
        //     data:res.locals
        // })
}

export {sendImageHandler}