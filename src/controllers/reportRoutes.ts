import { Router, Request, Response } from 'express';
import {requiresAuth} from "./auth/authController";
import {
    reportsController
} from "./reports"
import {responseHandler, sendImageHandler} from "./handlers";
import {debug} from "../config";


const log = debug('resizeRoutes');
debug.enable('resizeRoutes');

const router: Router = Router();

router.route('/').get(reportsController, responseHandler)

export const ReportsApi: Router = router;
