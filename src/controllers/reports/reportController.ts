import { Request, Response, NextFunction } from 'express';
import {devConfig,debug, Util} from "../../config"

import * as fs from "fs"
const log = debug('reportController');

 

const reportsController =  async function(req: Request, res:Response, next:NextFunction){
    
    debug.enable('reportController');
    try{
        
        let filesInLibrary = <Array<string>> await Util.readFilesInLibrary(devConfig.imageLibraryPath)
        let resizedFiles = <Array<string>> await Util.readFilesInLibrary(devConfig.resizedImagesPath) 
        res.locals.resizedFiles = resizedFiles.length;
        res.locals.originalFiles = filesInLibrary.length;
        res.locals.cache = {
            hits:devConfig.log.cacheHits,
            misses:devConfig.log.cacheMisses,
            total:devConfig.log.totalRequests
        }
        res.locals.success = true;
        log("Went through reportController")
        next()

    }catch(err){
        next(err);
    }
}



export {reportsController};
