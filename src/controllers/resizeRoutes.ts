import { Router, Request, Response } from 'express';
import {requiresAuth} from "./auth/authController";
import {
    resizeImageController,
    listItemsInLib
} from "./resize"
import {responseHandler, sendImageHandler} from "./handlers";
import {debug} from "../config";


const log = debug('resizeRoutes');
debug.enable('resizeRoutes');

const router: Router = Router();

router.route('/list').get(listItemsInLib, responseHandler)
router.route('/get/:imageName').get(resizeImageController, sendImageHandler)

export const ResizeApi: Router = router;
