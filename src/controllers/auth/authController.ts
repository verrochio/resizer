import { Router, Request, Response, NextFunction } from 'express';
import {debug} from "../../config";

const log = debug('auth');


const requiresAuth = function(req:Request, res:Response, next:NextFunction){
    debug.enable('auth')
    
    next()
    // if(authCondition=='1234'){
    //     log("went through auth");
    //     next()
    // }else throw(new Error("authentication failed"))
}


export {requiresAuth};